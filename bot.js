const Discord = require('discord.js');
const token = process.env.TOKEN;
const client = new Discord.Client();
const prefix = process.env.PREFIX;
const fs = require('fs');
const bot = client;

client.once('ready', () => {
	console.log('OverBot is online!');
  client.user.setActivity(`https://overthrow.ga/ - o!help | In ${client.guilds.size} servers!`, {type: "PLAYING"});
});


client.on('message', async message => {
const prefix = process.env.PREFIX
  const args = message.content.slice(prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  const msg = message;
  
    if(command === "ping") {

      const m = await message.channel.send("Ping?");
      m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
    }
  
  
  
  if(command === "kick") {

    if(!message.member.roles.some(r=>["Administrator", "Moderator", "Bots", "MineBot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    

    let member = message.mentions.members.first() || message.guild.members.get(args[0]);
    if(!member)
      return message.reply("Please mention a valid member of this server");
    if(!member.kickable) 
      return message.reply("I cannot kick this user! Do they have a higher role? Do I have kick permissions?");
    

    let reason = args.slice(1).join(' ');
    if(!reason) reason = "No reason provided";
    

    await member.kick(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't kick because of : ${error}`));
    message.reply(`${member.user.tag} has been kicked by ${message.author.tag} because: ${reason}`);

  }
  
  if(command === "ban") {

    if(!message.member.roles.some(r=>["Administrator"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    
    let member = message.mentions.members.first();
    if(!member)
      return message.reply("Please mention a valid member of this server");
    if(!member.bannable) 
      return message.reply("I cannot ban this user! Do they have a higher role? Do I have ban permissions?");

    let reason = args.slice(1).join(' ');
    if(!reason) reason = "No reason provided";
    
    await member.ban(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't ban because of : ${error}`));
    message.reply(`${member.user.tag} has been banned by ${message.author.tag} because: ${reason}`);
    msg.delete().catch(O_o=>{});
  }
  
  if(command === "purge") {

    const deleteCount = parseInt(args[0], 10);
    

    if(!deleteCount || deleteCount < 2 || deleteCount > 100)
      return message.reply("Please provide a number between 2 and 100 for the number of messages to delete");
    

    const fetched = await message.channel.fetchMessages({limit: deleteCount});
    message.channel.bulkDelete(fetched)
      .catch(error => message.reply(`Couldn't delete messages because of: ${error}`));
    msg.delete().catch(O_o=>{});
  }
  
    if(command === "say") {

    const sayMessage = args.join(" ");
  
    message.delete().catch(O_o=>{}); 

    message.channel.send(sayMessage);
  }
  
  if(command === "help") {
    msg.channel.send('```Here are the commands you can use:\n\nAdministrative Commands:\no!ban [member]: Bans a member within the server.\no!kick [member]: Kicks a member within the server\no!purge [number]: Purges messages, can purge up to 100 messages at a time.\n\nStandard Commands:\no!ping: Shows your latency.\no!say [message]: Sends a message and deletes yours.\no!nerd: No u!\no!flip: flop!\no!info: Gives you extra information about the bot.```');
    msg.delete().catch(O_o=>{});
  }
  
  if(command === "nerd") {
    msg.channel.send('No u!')
  }
  
  if(command === "info") {
    msg.channel.send('```Hello, I am OverBot. Created by OverThrow!\nI am a multipurpose bot for fun and moderation!\nIf you would like to see what commands you can run, type o!help\nFor more info about me vist https://overthrow.ga/overbot```');
  }
  
  if(command === "releasever") {
    msg.channel.send('Hello! I am OverBot, and I am officially released!\n In version 1.0, you will find the standard commands listed in o!help and https://overthrow.ga/overbot\n For more information about me, visit https://overthrow.ga/overbot or run m!info');
    msg.delete().catch(O_o=>{});
  }
  
if (message.content.startsWith("o!" + 'avatar')) {
    const user = message.mentions.users.first() || message.author;
    const avatarEmbed = new Discord.RichEmbed()
        .setColor(0x333333)
        .setAuthor(user.username)
        .setImage(user.avatarURL);
    message.channel.send(avatarEmbed);
}
  if(command === 'invite') {
    message.channel.send(`I see you want to invite me, <@${message.author.id}>! Here is the link: https://discordapp.com/oauth2/authorize?client_id=620252387092856842&scope=bot&permissions=268594238`);
  }
});

  

client.on('message', message => {
  const prefix = process.env.PREFIX
  const args = message.content.slice(prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  const msg = message;
  
  
	if (!message.content.startsWith(prefix) || message.author.bot) return;
  
  if (command === 'flip')
    msg.channel.send('flop')
  
  if(command === 'stop') {
    client.destroy();
    process.exit(0);
  }
});

client.login(token);

process.on('SIGINT', () => {
    client.destroy();
    process.exit(0);
});

process.on('SIGTERM', () => {
    client.destroy();
    process.exit(0);
});